create database if not exists compraventaDigital;

use compraventaDigital;

CREATE TABLE user (
	pk_id INT PRIMARY KEY,
	name varchar (255) not null,
	address varchar (255) not null,
	email varchar (255) not null,
	phone int default null,
	password varchar(255) not null,
    rating int not null,
	creation_date DATE,
	modification_date TIMESTAMP,
    profile_picture varchar(255) default null,
    province varchar(50) not null
);

CREATE TABLE catalogue (
	pk_id INT PRIMARY KEY,
	name varchar (255) not null,
	description varchar (255) not null,
	creation_date DATE,
	modification_date TIMESTAMP
);

CREATE TABLE product (
	pk_id INT PRIMARY KEY,
    id_section int,
    id_user int,
	name varchar (255) not null,
	description varchar (255) not null,
	price varchar(100) not null,
    creation_date DATE,
	modification_date TIMESTAMP,
    foreign key (id_section) references catalogue (pk_id),
    foreign key(id_user) references user (pk_id)
);

CREATE TABLE transactions (
	pk_id INT PRIMARY KEY,
    id_product int, 
    id_user int,
    description varchar (255) not null,
    rating int not null,
    creation_date DATE,
    modification_date TIMESTAMP,
    foreign key (id_product) references product (pk_id),
    foreign key (id_user) references user (pk_id)
);



